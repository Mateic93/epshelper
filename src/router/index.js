import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import AddTaskNote from "../views/AddTaskNote"
import ChangeStatus from "../views/ChangeStatus"
import TaskChangeStatus from "../views/TaskChangeStatus"
import CreateTask from "../views/CreateTask"
import UpdateSubject from "../views/UpdateSubject"
import SetRevisitLevel from "../views/SetRevisitLevel"
import AddAttachment from "../views/AddAttachment"
import AddPart from "../views/AddPart"
import AddSkill from "../views/AddSkill"
import CreateSR from "../views/CreateSR"
import CreatePISR from "../views/CreatePISR"
import MoveToFired from "../views/MoveToFired"
import SetNSVRepairAdvice from "../views/SetNSVRepairAdvice"
import CreateSRTask from "../views/CreateSRTask"
import CreateSRTaskREF from "../views/CreateSRTaskREF"
import SRLink from "../views/SRLink"
import SendMail from "../views/SendMail"
import UpdatePriority from "../views/UpdatePriority"
import UpdateSRType from "../views/UpdateSRType"
import AddNPP from "../views/AddNPP"
import UpdateTaskType from "../views/UpdateTaskType"
import UpdateTaskOwner from "../views/UpdateTaskOwner"
import UpdateSRGroup from "../views/UpdateSRGroup"
import NoAction from "../views/NoAction"
import Abbre from "../views/Short_abbreviation"
import CSV from "../views/CSV"
import test_html from "../views/test_html"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: "/AddTaskNote",
    name: "AddTaskNote",
    component: AddTaskNote
  },
  {
    path: "/ChangeStatus",
    name: "ChangeStatus",
    component: ChangeStatus
  },
  {
    path: "/TaskChangeStatus",
    name: "TaskChangeStatus",
    component: TaskChangeStatus
  },
  {
    path: "/CreateTask",
    name: "CreateTask",
    component: CreateTask
  },
  {
    path: "/UpdateSubject",
    name: "UpdateSubject",
    component: UpdateSubject
  },
  {
    path: "/SetRevisitLevel",
    name: "SetRevisitLevel",
    component: SetRevisitLevel
  },
  {
    path: "/AddAttachment",
    name: "AddAttachment",
    component: AddAttachment
  },
  {
    path: "/AddPart",
    name: "AddPart",
    component: AddPart
  },
  {
    path: "/AddSkill",
    name: "AddSkill",
    component: AddSkill
  },
  {
    path: "/CreateSR",
    name: "CreateSR",
    component: CreateSR
  },
  {
    path: "/CreatePISR",
    name: "CreatePISR",
    component: CreatePISR
  },
  {
    path: "/MoveToFired",
    name: "MoveToFired",
    component: MoveToFired
  },
  {
    path: "/SetNSVRepairAdvice",
    name: "SetNSVRepairAdvice",
    component: SetNSVRepairAdvice
  },
  {
    path: "/CreateSRTask",
    name: "CreateSRTask",
    component: CreateSRTask
  },
  {
    path: "/CreateSRTaskREF",
    name: "CreateSRTaskREF",
    component: CreateSRTaskREF
  },
  {
    path: "/SRLink",
    name: "SRLink",
    component: SRLink
  },
  {
    path: "/SendMail",
    name: "SendMail",
    component: SendMail
  },
  {
    path: "/UpdatePriority",
    name: "UpdatePriority",
    component: UpdatePriority
  },
  {
    path: "/UpdateSRType",
    name: "UpdateSRType",
    component: UpdateSRType
  },
  {
    path: "/AddNPP",
    name: "AddNPP",
    component: AddNPP
  },
  {
    path: "/UpdateTaskType",
    name: "UpdateTaskType",
    component: UpdateTaskType
  },
  {
    path: "/UpdateTaskOwner",
    name: "UpdateTaskOwner",
    component: UpdateTaskOwner
  },
  {
    path: "/UpdateSRGroup",
    name: "UpdateSRGroup",
    component: UpdateSRGroup
  },
  {
    path: "/NoAction",
    name: "NoAction",
    component: NoAction
  },
  {
    path: "/abbre",
    name: "abbre",
    component: Abbre
  },
  {
    path: "/csv",
    name: "csv",
    component: CSV
  },
  {
    path: "/test_html",
    name: "test_html",
    component: test_html
  }
  
  
]

const router = new VueRouter({
  routes
})

export default router
